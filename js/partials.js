///<reference path="node_modules/@types/jquery/index.d.ts" />
///<reference path="node_modules/@types/handlebars/index.d.ts" />

(function ($) {
    'use strict';
    //form login submit

    $('#form_newWl button').on('click', async function (e) {
        e.preventDefault();
        console.log('form_newWl');
        const jwt = sessionStorage.getItem('spect-jwt') ? sessionStorage.getItem('spect-jwt') : localStorage.getItem('spect-jwt');
        let data = JSON.stringify({
            jwt: jwt,
            newWl_label: $('#newWl_label').val()
        })
        let url = BASE_URL + 'wishlist.php';
        const response = await apiFilm.post(url, data, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
        const res = response.data;
        if (res.code != 200) {
            var html = data.message;
            console.log('error ' + html);
            let divErr = $('<hr><div class="alert alert-danger">' + html + '</div>');
            divErr.insertAfter($('#form_newWl'));
        } else {
            let divInsert = `
            <div class="archive-catagory-view mb-50 d-flex align-items-center justify-content-between" id="div_Wishlist_`+ res.data.id + `">
                    <!-- Catagory -->
                    <div class="archive-catagory">
                        <h4><i class="fa fa-trophy" aria-hidden="true"></i> `+ res.data.label + `</h4>
                    </div>
                    <div class="view-options">
                      <a href="#" data-href="`+ res.data.label + `" data-toggle="modal" data-target="#confirm-delete"
                      data-onclick="`+ res.data.id + `"
                      class="pull-right">
                      <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </div>
                </div>
            `;
            $('#div_Wishlist').append(divInsert);
            $('#form_newWl')[0].reset();
            //refresh
            checkUserConnected(jwt);
        }

        return false;
    });

    $('#confirm-delete').on('show.bs.modal', function (e) {
        //$(this).find('.btn-ok').attr('data-onclick', $(e.relatedTarget).data('onclick'));
        $('.debug-url').html('La collection: <strong>' + $(e.relatedTarget).data('href') + ' </strong> sera supprimée.');
        $('.btn-ok', this).data('onclick',  $(e.relatedTarget).data('onclick'));
    });
    // Bind click to OK button within popup
    $('#confirm-delete').on('click', '.btn-ok', async function (e) {
        e.preventDefault();
        let idWishlist = $('#btn_rmWl').data('onclick');
        console.log('removeWishlist', $(e.target).data('onclick'));
        
        const jwt = sessionStorage.getItem('spect-jwt') ? sessionStorage.getItem('spect-jwt') : localStorage.getItem('spect-jwt');
        console.log('params', idWishlist);
        let data = JSON.stringify({
            jwt: jwt,
            idWishlist: idWishlist
        })
        const response = await apiFilm.delete('wishlist.php',
            {
                data: {
                    jwt: jwt,
                    idWishlist: idWishlist
                }
            }, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        const res = response.data;
        if (res.code == 200) {
            $('#div_Wishlist_' + idWishlist).remove();
            $('#div_Wishlist_line_' + idWishlist).remove();
            
            $('#confirm-delete').modal('hide');
            //refresh
            checkUserConnected(jwt);
        } else {
            console.log('no ok');
        }
    });



    //form login submit
    $('#form_login button').on('click', function (e) {
        e.preventDefault();
        // login
        let url = BASE_URL + 'login.php';
        if ($(this).attr('value') == 'login-create')
            url = BASE_URL + 'spectator.php';
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify({
                email: $('#login-email').val(),
                password: $('#login-password').val()
            }),
            success: function (data) {
                if (data.code != 200) {
                    var html = data.message;
                    $('#result-login').addClass('alert alert-danger');
                    $('#result-login').html(html);
                } else {
                    if (data.userid) {
                        if ($('#login-remember').is(':checked')) {
                            localStorage.setItem('spect-jwt', data.jwt);
                            sessionStorage.setItem('spect-jwt', data.jwt);
                        } else {
                            sessionStorage.setItem('spect-jwt', data.jwt);
                        }
                        $('#form_login')[0].reset();
                        router.navigateTo('/profile');
                    }

                }
            },
            error: function (err) {
                console.log(err);
            }
        });

        return false;
    });

    //form post review submit
    $('#form_postReview').on('submit', function (e) {
        e.preventDefault();
        // login
        let url = BASE_URL + 'review.php';
        const jwt = sessionStorage.getItem('spect-jwt') ? sessionStorage.getItem('spect-jwt') : localStorage.getItem('spect-jwt');
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify({
                jwt: jwt,
                idFilm: $('#review-idFilm').val(),
                comment: $('#review-message').val()
            }),
            success: function (data) {
                if (data.code != 200) {
                    var html = data.message;
                    console.log('error ' + html);
                    let divErr = $('<hr><div class="alert alert-danger">' + html + '</div>');
                    divErr.insertAfter($('#form_postReview'));
                } else {
                    const review = data.data;
                    console.log(review);
                    if (review) {
                        let updated_at = formatDate(review['updated_at'], 'long');
                        $('#review-list').append(`
                <li class="single_comment_area">
                                    <!-- Comment Content -->
                                    <div class="comment-content d-flex">
                                        <!-- Comment Author -->
                                        <div class="comment-author">
                                            <img src="/image/spectators/`+ review['avatar'] + `" 
                                            style="border-radius: 50%;height: auto;max-width:70px;vertical-align: middle;border-style: none;"
                                                alt="Missing Image"
                                                onerror="this.src='/image/nouser.png';">
                                        </div>
                                        <!-- Comment Meta -->
                                        <div class="comment-meta">
                                            <a href="#" class="comment-date">`+ updated_at + `</a>
                                            <h6>`+ review['name'] + `</h6>
                                            <p>`+ review['comments'] + `</p>
                                        </div>
                                    </div>
                                </li>
                `);
                        console.log('is updated');

                        $('#form_postReview')[0].reset();
                    }

                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });



})(jQuery);
