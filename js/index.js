///<reference path="node_modules/@types/jquery/index.d.ts" />
///<reference path="node_modules/@types/handlebars/index.d.ts" />
//const BASE_URL = 'http://localhost/thymovie-rest/rest/';
const BASE_URL = 'http://rest.thymovie.com/rest/';
const BASE_IMG = 'http://thymovie.com';
const apiFilm = axios.create({
    baseURL: BASE_URL,
    timeout: 50000,
});

var menuBest = new Array();
var menuAtCinema = new Array();
var menuPreview = new Array();
var menuBestSerie = new Array();
var router;
var spectConnected;
/*Film actuellement au cinema*****************/
/*Meilleurs films*****************/
/*Film actuellement au cinema*****************/
window.addEventListener('load', () => {


    /**
     * traitement spécifique pour les écrans <800
     */
    if ($(window).innerWidth() <= 768) {
        $('.classynav a').attr('onclick', 'handleTouch(event)');
        $('.header-area .vizew-main-menu .classy-navbar form input').css('width', '60%');
    }


    const el = $('#app');

    // Display Error Banner
    const showError = (error) => {
        console.log(error);
        const html = Handlebars.templates.errorTmpl({
            color: 'red', error: 'error ', title: 'Une erreur est survenue - Veuillez réessayer ultérieurement!', message: error
        });
        el.html(html);
    };
    //router configuration
    router = new Router({
        mode: 'history',
        page404: (path) => {
            const html = Handlebars.templates.errorTmpl({
                title: 'Error 404 - Page introuvable!',
                message: `La page '/${path}' n'existe pas sur notre site`
            });
            el.html(html);
        },
    });
    const jwt = sessionStorage.getItem('spect-jwt') ? sessionStorage.getItem('spect-jwt') : localStorage.getItem('spect-jwt');
    checkUserConnected(jwt);
    /*******************************************************/
    /********************** /home or / **********************/
    /*******************************************************/
    const homeMiddleware = async () => {
        try {
            if (menuBest.length == 0 || menuAtCinema.length == 0 ||
                menuPreview.length == 0 || menuBestSerie.length == 0) {
                const jsonFilmList = await apiFilm.get('film.php/menuHome');
                jsonFilmList.data.forEach(element => {
                    switch (element['bests']) {
                        case 'BESTS':
                            menuBest.push(element);
                            break;
                        case 'ATCINEMA':
                            menuAtCinema.push(element);
                            break;
                        case 'PREVIEW':
                            menuPreview.push(element);
                            break;
                        case 'BESTSERIE':
                            menuBestSerie.push(element);
                            break;
                    }
                });
            }
            html = Handlebars.templates.homeTmpl({ menuBest: menuBest, menuAtCinema: menuAtCinema, menuPreview: menuPreview, menuBestSerie: menuBestSerie, routerWith: 'router', BASE_IMG: BASE_IMG });
            el.html(html);
        } catch (error) {
            showError(error);
        } finally {
            appendScript('/js/popper.min.js');
            appendScript('/js/plugins/plugins.js');
            appendScript('/js/active.js');
            appendScript('/js/partials.js');
            $('html, body').animate({
                scrollTop: 0
            }, 800);
        }

    }
    router.add('/', function () {
        homeMiddleware();
        console.log('Fin / ');
    });
    router.add('/home', function () {
        homeMiddleware();
        console.log('Fin /home ');
    });


    /*******************************************************/
    /********************** /films **********************/
    /*******************************************************/
    const filmsMiddleware = async (filmType, name, param, paramStr) => {
        showLoader();
        var dataPost = null;
        let displayFilter = true;
        if (name !== '' && param !== '') {
            dataPost = {
                name: name,
                param: param
            };
        } else if (param !== '') {
            dataPost = {
                param: param
            };
        }

        var filmList = new Array();
        var sortFilmConf = new Array();
        let urlSort = BASE_URL + 'configsort.php/' + filmType;
        try {
            // Requests will be executed in parallel...
            axios.all([
                apiFilm.get(urlSort)
            ]).then(axios.spread(function (sortFilmConfRes) {
                //... but this callback will be executed only when both requests are complete.
                //filmList = filmListRes.data;
                sortFilmConf = sortFilmConfRes.data;
                let linkName = 'Tous les films';
                if (filmType == 'serie') {
                    linkName = 'Toutes les séries';
                }
                else if (filmType == 'all') {
                    if (paramStr)
                        linkName = 'Résultat(s) pour : ' + paramStr;
                    else
                        linkName = 'Résultat(s) pour : ' + param;
                    displayFilter = false;
                }
                if ($(window).innerWidth() <= 768) {
                    displayFilter = false;
                }
                html = Handlebars.templates.filmsTmpl({ filmList: filmList, sortFilmConf: sortFilmConf, filmType: filmType, linkName: linkName, BASE_IMG: BASE_IMG });
                el.html(html);
                $('#example').dataTable({
                    "serverSide": true,
                    "pagingType": "full_numbers",
                    "info": false,
                    "bLengthChange": false,
                    "processing": true,
                    "bFilter": displayFilter,
                    "oLanguage": {
                        sProcessing: "Chargement...",
                        "oPaginate": {
                            "sFirst": "«", // This is the link to the first page
                            "sPrevious": '<i class="fa fa-angle-left"></i>', // This is the link to the previous page
                            "sNext": '<i class="fa fa-angle-right"></i>', // This is the link to the next page
                            "sLast": "»" // This is the link to the last page
                        },
                        "sEmptyTable": "Aucun résultat retourné",
                        "sSearch": "Recherche:"
                    },
                    "ajax": {
                        url: BASE_URL + "film_data.php",
                        type: "POST",
                        //data:dataPost,
                        data: function (data) {
                            data.filmType = filmType;
                            if (dataPost) {
                                data.name = dataPost.name;
                                data.param = dataPost.param;
                            }
                        },
                        error: function () {
                            $("#post_list_processing").css("display", "none");
                            hideLoader();
                        },
                        complete: function () {
                            appendScript('/js/popper.min.js');
                            appendScript('/js/plugins/plugins.js');
                            appendScript('/js/active.js');
                            appendScript('/js/partials.js');

                            $('html, body').animate({
                                scrollTop: 0
                            }, 800);
                            hideLoader();
                        }
                    },
                    fnDrawCallback: function () {
                        $("#example thead").remove();
                    },
                    "columns": [
                        { "ID": "ID" }
                    ],
                    "columnDefs": [{
                        "targets": 0,
                        "data": "download_link",
                        "render": function (data, type, row, meta) {
                            let synopsis = row[2];
                            if (synopsis && synopsis.length > 200)
                                synopsis = synopsis.substr(0, 200) + '...';
                            let imageName = row[17];
                            let imagePath = BASE_IMG + '/image/med';
                            if (imageName == '0') {
                                imagePath = BASE_IMG + '/image/large';
                                imageName = row[18];
                            }
                            let folderImg = '/thymovies-img/';
                            let filmDuration = ' <i class="fa fa-clock-o" aria-hidden="true"></i><a href="#" class="post-date"> &nbsp;' + formatDuration(row[3]) + ' </a>';
                            if (row[13] == 12) {
                                folderImg = '/thymovies-serie-img/';
                                filmDuration = '';
                            }
                            return `
                        <div class="single-post-area style-2">
                            <div class="row align-items-center">
                               <div class="col-6 col-sm-5 col-md-4 col-lg-4">
                                    <div class="post-thumbnail">
                                      <img class="img-fluid" alt="Missing Image" onerror="this.src='http://thymovie.com/image/noimage.png';" width="250" src="`+ imagePath + folderImg + row[11] + `/` + imageName + `" />
                                    </div>
                                </div>
                                <div class="col-6 col-sm-7 col-md-8 col-lg-8">
                                     <!-- Post Content -->
                                    <div class="post-content mt-0">
                                        <a href="#" aria-hidden="true" class="post-cata cata-sm cata-success">`+ row[16] + `</a>
                                        <a onclick="event.preventDefault();router.navigateTo('/films/`+ row[0] + `/` + convertToBeautyUrl(row[1]) + `')")"
                                           href="#" class="post-title">` + row[1] + `</a>
                                        <div aria-hidden="true" class="post-meta d-flex align-items-center mb-2">
                                            <a href="#" class="post-author">Année de production: `+ row[4] + `</a>
                                            `+ filmDuration + `
                                        </div>
                                        <p class="mb-2">`+ synopsis + `</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                         `;
                        }
                    }
                    ]
                });
            }));

        } catch (error) {
            showError(error);
        } finally {
            //hideLoader();
        }
    }
    /**/
    router.add('/films', function () {
        filmsMiddleware('film', '', '');
        console.log('Fin /films ');
    });
    router.add('/films/{filmType}/{name}/{param}/{paramStr}', function (filmType, name, param, paramStr) {
        filmsMiddleware(filmType, name, param, paramStr);
    });
    router.add('/films/{filmType}/{name}/{param}', function (filmType, name, param) {
        filmsMiddleware(filmType, name, param, null);
    });
    router.add('/films/findBykeyword/{param}', function (param) {
        filmsMiddleware('all', '', param, null);
    });
    router.add('/series', function () {
        filmsMiddleware('serie', '', '', null);
        console.log('Fin', 'serie');
    });

    /*******************************************************/
    /********************** /filmDetail **********************/
    /*******************************************************/
    const filmDetailMiddleware = async (idFilm, title) => {
        showLoader();
        try {
            const jsonDetail = await apiFilm.get('film.php/' + idFilm);
            const filmDetail = jsonDetail.data;
            let folderImg = 'thymovies-img';
            let filmType = 'film';
            let linkName = 'Tous les films';
            if (jsonDetail.data['film_type_id'] == 12) {
                folderImg = 'thymovies-serie-img';
                filmType = 'serie';
                linkName = 'Toutes les séries';
            }
            let labelWishlist = null;
            if (spectConnected) {
                if (spectConnected.wishlists) {
                    for (let ws of spectConnected.wishlists) {
                        if (labelWishlist)
                            break;
                        let wishlistLines = ws.wistlist_lines;
                        if (wishlistLines) {
                            for (let line of wishlistLines) {
                                if (filmDetail.ID == line.ID) {
                                    labelWishlist = ws.LABEL;
                                    break;
                                }
                            }

                        }
                    }

                }
            }
            html = Handlebars.templates.filmDetailTmpl({
                filmDetail: filmDetail,
                filmPrev: filmDetail.film_suggest[0],
                filmNext: filmDetail.film_suggest[1],
                linkToFilms: '/' + filmType + 's',
                linkName: linkName,
                folderImg: folderImg,
                filmType: filmType,
                spectator: spectConnected,
                labelWishlist: labelWishlist,
                BASE_IMG: BASE_IMG
            });
            el.html(html);
        } catch (error) {
            showError(error);
        } finally {
            appendScript('/js/popper.min.js');
            appendScript('/js/plugins/plugins.js');
            appendScript('/js/active.js');
            appendScript('/js/partials.js');
            hideLoader();
            $('html, body').animate({
                scrollTop: 0
            }, 800);
        }
    };
    router.add('films/{idFilm}', function (idFilm) {
        filmDetailMiddleware(idFilm, '');
    });
    router.add('films/{idFilm}/{title}', function (idFilm, title) {
        filmDetailMiddleware(idFilm, title);
    });

    /*******************************************************/
    /********************** /login    **********************/
    /*******************************************************/
    const loginMiddleware = async () => {
        showLoader();
        try {
            let html = Handlebars.templates.loginTmpl();
            const jwt = sessionStorage.getItem('spect-jwt') ? sessionStorage.getItem('spect-jwt') : localStorage.getItem('spect-jwt');
            if (jwt) {
                const res = await apiFilm.get('login.php',
                    {
                        params: {
                            jwt: jwt
                        }
                    }
                );
                const jsonUser = res.data;
                if (jsonUser.code == 200) {
                    const spectator = jsonUser.data;
                    /*  let newWishlist = Array();
                     spectator.wishlists.forEach(ws => {
                         let newWl_Line = constructListOfRow(ws.wistlist_lines, 4);
                         ws.wistlist_lines = newWl_Line
                     }); */
                    spectConnected = spectator;
                    $('#button-login').hide();
                    $('#connectedDiv').show();



                    html = Handlebars.templates.profileTmpl({
                        spectator: spectator,
                        BASE_IMG: BASE_IMG
                    });
                    el.html(html);
                } else {
                    $('#connectedDiv').hide();
                    $('#button-login').show();
                }
            } else {
                $('#connectedDiv').hide();
                $('#button-login').show();
            }
            el.html(html);
        } catch (error) {
            showError(error);
        } finally {
            hideLoader();
            appendScript('/js/partials.js');
            $('html, body').animate({
                scrollTop: 0
            }, 800);
        }

    };
    router.add('/login', function () {
        loginMiddleware();
    });
    /*******************************************************/
    /********************** /login    **********************/
    /*******************************************************/
    router.add('/profile', function () {
        loginMiddleware();
    });

    // navigate app to current url
    router.navigateTo(window.localStorage.pathname);
    //highlight active menu on refresh/page reload
    const link = $(`a[href$='${window.location.pathname}]`);
    link.addBack('active');


    $(document).on('click touchstart tap', 'a', function (event) {
        navLinkTo(event);
    });

    /**
    
     var closeIcon = $('.classycloseIcon');
     var classyMenu = $('.classy-menu');
                 // close icon
                closeIcon.on('click', function () {
                    classyMenu.removeClass('menu-on');
                    navToggler.removeClass('active');
                }); 
     
     */

    /*     $('a').on('click', (event) => {
        }); */
    //spinner loading
    function showLoader() {
        var divLoad = $('<div/>', {
            id: 'myLoader',
            html: ` <div class="preloader d-flex align-items-center justify-content-center">
            <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
          </div>`
        });
        divLoad.appendTo($('header'));
    }
    function hideLoader() {
        $('#myLoader').remove();
    }


    $('.preloader').fadeOut('slow', function () {
        $(this).remove();
    });

});
/****************************************** */
const checkUserConnected = async (jwt) => {
    if (jwt) {
        const res = await apiFilm.get('login.php',
            {
                params: {
                    jwt: jwt
                }
            }
        );
        const jsonUser = res.data;
        if (jsonUser.code == 200) {
            const spectator = jsonUser.data;
            spectConnected = spectator;
            //hide login button
            $('#button-login').hide();
            $('#connectedDiv').show();
        } else {
            $('#connectedDiv').hide();
            $('#button-login').show();
        }
    } else {
        $('#connectedDiv').hide();
        $('#button-login').show();
    }
};
var DateFormats = {
    short: "DD MMMM - YYYY",
    long: "dddd DD.MM.YYYY HH:mm"
}
function formatDate(datetime, format) {
    if (moment) {
        format = DateFormats[format] || format;
        return moment(datetime).locale('fr').format(format);
    }
    else {
        return datetime;
    }
}
function formatDuration(duration) {
    if (duration && duration !== '0') {
        let totalMinutes = parseInt(duration);
        let hour = parseInt(totalMinutes / 60);
        let min = parseInt(totalMinutes % 60);
        return hour + 'h ' + (min > 10 ? min : '0' + min);
    }
    return '';
}
function convertToBeautyUrl(text) {
    const a = 'àáäâãèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
    const b = 'aaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
    const p = new RegExp(a.split('').join('|'), 'g')
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(p, c =>
            b.charAt(a.indexOf(c)))     // Replace special chars
        .replace(/&/g, '-and-')         // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '')             // Trim - from end of text
}

window.onpopstate = function (event) {
    let path = document.location.pathname;
    router.navigateTo(path);
};

/* Warning before leaving the page (back button, or outgoinglink) */
window.addEventListener
    ('beforeunload'
        , function (evt) {
            evt.returnValue = 'Hello';
            return evt.returnValue;

        }
    );
window.addEventListener
    ('unload'
        , function (evt) {
            evt.preventDefault();
        }
    );

function appendScript(filepath) {

    if ($('body script[src="' + filepath + '"]').length > 0)
        $('body script[src="' + filepath + '"]').remove();
    //return;

    var ele = document.createElement('script');
    ele.setAttribute("src", filepath);
    $('body').append(ele);
}

$('form').submit(function (evt) {
    evt.preventDefault();
    const keyword = evt.target.elements['top-search'].value;
    if (keyword) {
        router.navigateTo('films/findBykeyword/' + convertToBeautyUrl(keyword));
    }
});

$('input.typeahead').typeahead({
    source: function (query, process) {
        if (!query || query.length < 3)
            return;
        $.ajax({
            url: BASE_URL + 'film.php/keyword/' + query,
            type: "GET",
            dataType: 'JSON',
            async: true,
            success: function (data) {
                var resultList = data.map(function (item) {
                    var link = { id: item.id, title: item.title };
                    return JSON.stringify(link);
                });
                return process(resultList);
            }
        })
    },
    matcher: function (obj) {
        var item = JSON.parse(obj);
        return item.title.toLowerCase().indexOf(this.query.toLowerCase())
    },
    sorter: function (items) {
        var beginswith = [], caseSensitive = [], caseInsensitive = [], item;
        while (link = items.shift()) {
            var item = JSON.parse(link);
            if (!item.title.toLowerCase().indexOf(this.query.toLowerCase())) beginswith.push(JSON.stringify(item));
            else if (item.title.indexOf(this.query)) caseSensitive.push(JSON.stringify(item));
            else caseInsensitive.push(JSON.stringify(item));
        }
        return beginswith.concat(caseSensitive, caseInsensitive)
    },
    highlighter: function (link) {
        var item = JSON.parse(link);
        html = '<div><div class="typeahead-inner" id="' + item.id + '">';
        html += '<a onclick="event.preventDefault();router.navigateTo(\'/films/' + item.id + '/' + convertToBeautyUrl(item.title) + '\')" href="#">' + item.title + ' </a>';
        html += '</div></div>';

        var reEscQuery = this.query.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
        var reQuery = new RegExp('(' + reEscQuery + ')', "gi");
        var jElem = $(html);
        var textNodes = $(jElem.find('*')).add(jElem).contents().filter(function () {
            return this.nodeType === 3;
        });
        textNodes.replaceWith(function () {
            return $(this).text().replace(reQuery, '<strong>$1</strong>');
        });

        return jElem.html();
    },
    updater: function (link) {
        var item = JSON.parse(link);
        router.navigateTo('films/' + item.id);
        //return item.title;  
    }
});

function constructListOfRow(listItem, nbElmByRow) {
    let rows = Array();
    let nbColumn = parseInt(12 / nbElmByRow);
    for (let idx = 0; idx < listItem.length; idx += nbColumn) {
        let idxTmp = idx;
        let row = Array();
        for (let i = 0; i < nbColumn; i++) {
            if (listItem[idxTmp])
                row.push(listItem[idxTmp]);
            idxTmp++;
        }
        rows.push(row);
    }
    return rows;

}

$('#btn-disconnect').on('click', function (e) {
    e.preventDefault();
    sessionStorage.setItem('spect-jwt', null);
    localStorage.setItem('spect-jwt', null);
    spectConnected = null;
    $('#button-login').show();
    $('#connectedDiv').hide();
    router.navigateTo('/home');
});

addToMyList = async (e, idWistlist, idFilm) => {
    e.preventDefault();
    console.log('params', idWistlist, idFilm);
    const jwt = sessionStorage.getItem('spect-jwt') ? sessionStorage.getItem('spect-jwt') : localStorage.getItem('spect-jwt');
    let data = JSON.stringify({
        jwt: jwt,
        idFilm: idFilm,
        idWistlist: idWistlist
    })
    const response = await apiFilm.post('wishlist_line.php', data, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
    const res = response.data;
    console.log('res ' + JSON.stringify(res));
    if (res.code == 200) {
        console.log('ok');
        let html = `
        <button class="btn btn-warning btn-sm" type="button">
                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                    Ce film est dans `+ res.data.label + `
                                </button>
        `;
        let divOk = $(html);

        divOk.insertBefore($('#div_AddToWistList'));
        $('#div_AddToWistList').hide();
        //update wishlist spectator connected
        const jwt = sessionStorage.getItem('spect-jwt') ? sessionStorage.getItem('spect-jwt') : localStorage.getItem('spect-jwt');
        checkUserConnected(jwt);
    } else {
        console.log('no ok');
    }
};
removeFilmFromWl = async (e, idWistlist_Line) => {
    e.preventDefault();
    const jwt = sessionStorage.getItem('spect-jwt') ? sessionStorage.getItem('spect-jwt') : localStorage.getItem('spect-jwt');
    console.log('params', idWistlist_Line);
    let data = JSON.stringify({
        jwt: jwt,
        idWistlist_Line: idWistlist_Line
    })
    const response = await apiFilm.delete('wishlist_line.php',
        {
            data: {
                jwt: jwt,
                idWistlist_Line: idWistlist_Line
            }
        }, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });
    const res = response.data;
    console.log('res ' + JSON.stringify(res));
    if (res.code == 200) {
        console.log('ok');
        $('#div_Wl_Film_' + idWistlist_Line).remove();
    } else {
        console.log('no ok');
    }
};

function handleTouch() {
    navLinkTo(event);
}
function navLinkTo(event) {
    //block browser page load
    event.preventDefault();
    let target = $(event.target);
    if ($(event.target).is('i') || $(event.target).is('img')) {
        target = $(event.target).parent();
    }
    if (target.attr('data-toggle'))
        return;
    //navigate to clicked url
    const href = target.attr('href');
    if (!href)
        return;
    if (href == '#')
        return;
    const path = href.substr(href.lastIndexOf('/'));
    //Highlight active Menu on Click
    target.addClass('active');
    if ($(window).innerWidth() <= 768) {
        $('.classy-menu').removeClass('menu-on');
    }
    router.navigateTo(path);
}