(function ($) {
    'use strict';

    //Helpers
    Handlebars.registerHelper('limitStr', function (text, limit) {
        let lim = parseInt(limit);
        if (text)
            return text.substr(0, lim);
        else
            return '';
    });
    Handlebars.registerHelper('if_eq', function (conditional, options) {
        if (conditional == options.hash.equals) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
    Handlebars.registerHelper('checklengthGreater', function (v1, v2, options) {
        'use strict';
        if (v1.length > v2) {
            return options.fn(this);
        }
        return options.inverse(this);
    });
    Handlebars.registerHelper('convertUrl', function (str) {
        if (str)
            return convertToBeautyUrl(str);
        else
            return '';
    });

    Handlebars.registerHelper("formatDuration", function (duration) {
        return formatDuration(duration);
    });
    Handlebars.registerHelper("formatDate", function (datetime, format) {
        return formatDate(datetime, format);
    });

    /* Handler to check multiple conditions
       */
    Handlebars.registerHelper('checkIf', function (v1, o1, v2, mainOperator, v3, o2, v4, options) {
        var operators = {
            '==': function (a, b) { return a == b },
            '===': function (a, b) { return a === b },
            '!=': function (a, b) { return a != b },
            '!==': function (a, b) { return a !== b },
            '<': function (a, b) { return a < b },
            '<=': function (a, b) { return a <= b },
            '>': function (a, b) { return a > b },
            '>=': function (a, b) { return a >= b },
            '&&': function (a, b) { return a && b },
            '||': function (a, b) { return a || b },
        }
        var a1 = operators[o1](v1, v2);
        var a2 = operators[o2](v3, v4);
        var isTrue = operators[mainOperator](a1, a2);
        return isTrue ? options.fn(this) : options.inverse(this);
    });

    /**************************************************/
    /********************            
            appendScript('/js/active.js');
            appendScript('/js/partials.js');
    **********************/


})(jQuery);
